#! -*- coding: utf-8 -*-

from trytond.pool import Pool
from .health_appointment_activity_type_fiuner import *

def register():
    Pool.register(
        Appointment,
        module='health_appointment_activity_type_fiuner', type_ = 'model' )
    
